package com.gnchiesa.cordova.plugin;
// The native Toast API

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.zebra.android.comm.BluetoothPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnectionException;
import com.zebra.android.printer.PrinterLanguage;
import com.zebra.android.printer.ZebraPrinter;
import com.zebra.android.printer.ZebraPrinterFactory;
import com.zebra.android.printer.ZebraPrinterLanguageUnknownException;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// Cordova-required packages

public class ZebraPrintPlugin extends CordovaPlugin {
  private static final String DURATION_LONG = "long";

    private static final String TAG = "ZEBRA";

    public static final byte ESC = 0x1B;
    public static final byte[] CHAR_SETTING = new byte[] {0x1B,0x23,0x23,0x53,0x4C,0x41,0x4E,0x12};
    public static final byte[] ESC_ENTER = new byte[] { 0x1B, 0x4A, 0x40 };
    private final static byte[] LINE_FEED = new byte[]{0x0A};
    private final static byte[] CR = new byte[]{0x0D};
    public static final byte[] ESC_CANCEL_BOLD = new byte[] { ESC, 0x21, 0 };
    public static final byte[] ESC_ENABLED_BOLD = new byte[] { ESC, 0x21, 8};
    public static final byte[] ESC_ENABLED_UNDERLINE = new byte[] { ESC, 0x2D, 0x01};
    public static final byte[] H1_SIZE = new byte[] { 0x1D, 0x21, 0x10 };
    public static final byte[] NORMAL_SIZE = new byte[] { 0x1D, 0x21, 0x00 };

    private Context context;

    private boolean conn = false;

    private static BluetoothAdapter btadapter;
    private ArrayList<BluetoothDevice> found_devices;
    private boolean discovering=false;

    private ZebraPrinterConnection zebraPrinterConnection;
    private ZebraPrinter printer;



  @Override
  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {

      PluginResult result = null;
      context = this.cordova.getActivity().getApplicationContext();

      // Register for broadcasts when a device is discovered
      IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
      context.registerReceiver(mReceiver, filter);

      // Register for broadcasts when discovery starts
      filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
      context.registerReceiver(mReceiver, filter);


      // Register for broadcasts when discovery has finished
      filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
      context.registerReceiver(mReceiver, filter);


      // Register for broadcasts when connectivity state changes
      filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
      context.registerReceiver(mReceiver, filter);


      //Looper.prepare();
      btadapter= BluetoothAdapter.getDefaultAdapter();
      found_devices=new ArrayList<BluetoothDevice>();


      try {

          String jsonArgs = args.getString(0);

          Gson gson = new Gson();

          Map<String,String> map = new HashMap<>();
          map = (Map<String,String>) gson.fromJson(jsonArgs, map.getClass());


          PrintInftoDTO printInftoDTO = gson.fromJson(map.get("message"),PrintInftoDTO.class);



          String text = printInftoDTO.getPrintText();
          String printetMAC = printInftoDTO.getPrinterMac();


//          String text = args.getString(0);
//          String imageText = args.getString(1);
//          String printetMAC = args.getString(2);
//          String jsonInfra = args.getString(3);

          try {
              boolean enabled = false;

              Log.d("BluetoothPlugin", "Enabling Bluetooth...");

              if (btadapter.isEnabled())
              {
                  enabled = true;
              } else {
                  enabled = btadapter.enable();
                  SystemClock.sleep(3000);
              }

              result = new PluginResult(PluginResult.Status.OK, enabled);
          } catch (Exception Ex) {
              result = new PluginResult(PluginResult.Status.ERROR);
          }

          System.out.println(action);

          CheckGC("onCreate_Start");

          printer = connect(printetMAC);
          //printer = connect("AC:3F:A4:10:80:66");
          CheckGC("onCreate_End");


          CheckGC("Connect_Start");
          //BxlService mBxlService = null;

          int returnValue;

          if (printer != null) {
              System.out.println("Conectado");
              conn = true;

              CheckGC("PrintText_Start");


              //zebraPrinterConnection.write(CHAR_SETTING);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              //zebraPrinterConnection.write(ESC_ENABLED_BOLD);

              zebraPrinterConnection.write("MUNICIPALIDAD DE CANADA DE GOMEZ".getBytes());
              //zebraPrinterConnection.write(ESC_CANCEL_BOLD);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(CR);

              zebraPrinterConnection.write(text.getBytes());
              if (zebraPrinterConnection instanceof BluetoothPrinterConnection) {
                  String friendlyName = ((BluetoothPrinterConnection) zebraPrinterConnection).getFriendlyName();
              }


              CheckGC("PrintText_End");

              //Print image
              CheckGC("PrintImage_Start");

              //ANDROID 4.1
              String image = "";

              CheckGC("PrintImage_End");

              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              //zebraPrinterConnection.write(salto.getBytes());
              String firmas = "---------------------------------------";

              zebraPrinterConnection.write(firmas.getBytes());
              zebraPrinterConnection.write(CR);
              //salto = "\\r\\n";
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);

              //zebraPrinterConnection.write(salto.getBytes());
              firmas = " FIRMA DEL INFRACTOR";
              zebraPrinterConnection.write(firmas.getBytes());

              zebraPrinterConnection.write(CR);
              //salto = "\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n";
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              //zebraPrinterConnection.write(salto.getBytes());
              firmas = "---------------------------------------";
              zebraPrinterConnection.write(firmas.getBytes());
              zebraPrinterConnection.write(CR);
              //salto = "\\r\\n";
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              //zebraPrinterConnection.write(salto.getBytes());
              //firmas = " INSPECTOR: " + barcodeDTO.getInspector();
              zebraPrinterConnection.write(firmas.getBytes());
              zebraPrinterConnection.write(CR);
              //salto = "\\r\\n";
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);

              String textoInsp = "Se fija en parabrisas Copia del Acta de";
              zebraPrinterConnection.write(textoInsp.getBytes());
              zebraPrinterConnection.write(CR);

              textoInsp = "Infraccion por no haber persona que se haga";
              zebraPrinterConnection.write(textoInsp.getBytes());
              zebraPrinterConnection.write(CR);

              textoInsp = "cargo de su Recepcion y no encontrarse el";
              zebraPrinterConnection.write(textoInsp.getBytes());
              zebraPrinterConnection.write(CR);

              textoInsp = "Infractor en el Lugar";
              zebraPrinterConnection.write(textoInsp.getBytes());
              zebraPrinterConnection.write(CR);

              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);
              zebraPrinterConnection.write(LINE_FEED);

              zebraPrinterConnection.close();
              result = new PluginResult(PluginResult.Status.OK,false);
          } else {
              conn = false;
              result = new PluginResult(PluginResult.Status.ERROR,"No se pudo conectar la impresora!");
          }
      } catch (Exception e) {
          e.printStackTrace();
      } finally {
          disconnect();
      }

      return true;



//      // Verify that the user sent a 'show' action
//      if (!action.equals("show")) {
//        callbackContext.error("\"" + action + "\" is not a recognized action.");
//        return false;
//      }
//      String message;
//      String duration;
//      try {
//        JSONObject options = args.getJSONObject(0);
//        message = org.apache.commons.lang3.StringUtils.upperCase(options.getString("message"));
//        duration = options.getString("duration");
//      } catch (JSONException e) {
//        callbackContext.error("Error encountered: " + e.getMessage());
//        return false;
//      }
//      // Create the toast
//      Toast toast = Toast.makeText(cordova.getActivity(), "the message" + message,
//        DURATION_LONG.equals(duration) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
//      // Display toast
//      toast.show();
//      // Send a positive result to the callbackContext
//      PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
//      callbackContext.sendPluginResult(pluginResult);
//      return true;
  }



    /** BroadcastReceiver to receive bluetooth events */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent)
        {

            String action = intent.getAction();
            Log.i("BluetoothPlugin","Action: "+action);

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    addDevice(device);
                }else{
//                    Log.i("BluetoothPlugin","Device already paired");
                }

                // When discovery starts
            }else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {

                LOG.i("BluetoothPlugin","Discovery started");
                setDiscovering(true);

                // When discovery finishes
            }else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {

                Log.i("BluetoothPlugin","Discovery finilized");
                setDiscovering(false);

            }
        }
    };

    public void setDiscovering(boolean state){
        discovering=state;
    }



    public void addDevice(BluetoothDevice device){
        if (!found_devices.contains(device))
        {
            found_devices.add(device);
        }
    }


    /**
     */
    @Override
    public void onPause(boolean multitasking)
    {
        super.onPause(multitasking);
    }


    /**
     */
    @Override
    public void onResume(boolean multitasking)
    {
        super.onResume(multitasking);
    }


    /**
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();

    }

    void CheckGC(String FunctionName) {
        long VmfreeMemory = Runtime.getRuntime().freeMemory();
        long VmmaxMemory = Runtime.getRuntime().maxMemory();
        long VmtotalMemory = Runtime.getRuntime().totalMemory();
        long Memorypercentage = ((VmtotalMemory - VmfreeMemory) * 100) / VmtotalMemory;

        Log.i(TAG, FunctionName + "Before Memorypercentage" + Memorypercentage + "% VmtotalMemory["
                + VmtotalMemory + "] " + "VmfreeMemory[" + VmfreeMemory + "] " + "VmmaxMemory[" + VmmaxMemory
                + "] ");

        // Runtime.getRuntime().gc();
        System.runFinalization();
        System.gc();
        VmfreeMemory = Runtime.getRuntime().freeMemory();
        VmmaxMemory = Runtime.getRuntime().maxMemory();
        VmtotalMemory = Runtime.getRuntime().totalMemory();
        Memorypercentage = ((VmtotalMemory - VmfreeMemory) * 100) / VmtotalMemory;

        Log.i(TAG, FunctionName + "_After Memorypercentage" + Memorypercentage + "% VmtotalMemory["
                + VmtotalMemory + "] " + "VmfreeMemory[" + VmfreeMemory + "] " + "VmmaxMemory[" + VmmaxMemory
                + "] ");
    }

    public void PrintOutput(String FunctionName, byte[] Data) {
        for (int i = 0; i < Data.length; i++) {
            Log.i(TAG, FunctionName + "Data[" + i + "]=" + Data[i]);
        }
    }



    public ZebraPrinter connect(String macadress) {
//        setStatus("Connecting...", Color.YELLOW);
        zebraPrinterConnection = null;
        zebraPrinterConnection = new BluetoothPrinterConnection(macadress);
        SettingsHelper.saveBluetoothAddress( context.getApplicationContext().getApplicationContext(), macadress);

        try {
            zebraPrinterConnection.open();
        } catch (ZebraPrinterConnectionException e) {
            disconnect();
        }

        ZebraPrinter printer = null;

        if (zebraPrinterConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(zebraPrinterConnection);
//                setStatus("Determining Printer Language", Color.YELLOW);
                PrinterLanguage pl = printer.getPrinterControlLanguage();
//                setStatus("Printer Language " + pl, Color.BLUE);
            } catch (ZebraPrinterConnectionException e) {
//                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
//                DemoSleeper.sleep(1000);
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
//                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
//                DemoSleeper.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }

    public void disconnect() {
        try {
            if (zebraPrinterConnection != null) {
                zebraPrinterConnection.close();
            }
        } catch (ZebraPrinterConnectionException e) {
        } finally {
            //enableTestButton(true);
        }
    }

}