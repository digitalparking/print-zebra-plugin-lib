// Empty constructor
function ZebraPrintPlugin() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
ZebraPrintPlugin.prototype.print = function(message, successCallback, errorCallback) {
  var options = {};
  options.message = message;
  //options.duration = duration;
  cordova.exec(successCallback, errorCallback, 'ZebraPrintPlugin', 'print', [options]);
}

// Installation constructor that binds MiPlugin to window
ZebraPrintPlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.zebraPrintPlugin = new ZebraPrintPlugin();
  return window.plugins.zebraPrintPlugin;
};
cordova.addConstructor(ZebraPrintPlugin.install);
